<?php

namespace dlouhy\BlogBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use dlouhy\BlogBundle\Entity\Post;

class PostRepository extends EntityRepository
{


	/**
	 * @param int $limit
	 * @return Post[]|false
	 */
	public function findAllOrdered($limit = null)
	{
		return $this->getEntityManager()
						->createQuery(
								'SELECT p FROM dlouhy\BlogBundle\Entity\Post p WHERE '
								. 'p.active = 1 AND '
								. 'p.deleted = 0 AND '
								. 'p.publishedFrom <= CURRENT_TIMESTAMP() '
								. 'ORDER BY p.date DESC '
						)
						->setMaxResults($limit)
						->getResult();
	}


	/**
	 * @return QueryBuilder
	 */
	public function getQueryPublishedPosts()
	{
		return $this->createQueryBuilder('p')
						->where('p.active = :true')
						->andWhere('p.deleted >= :false')
						->andWhere('p.publishedAt <= :now')
						->setParameter('true', true)
						->setParameter('false', false)
						->setParameter('now', new \DateTime)
						->orderBy('p.publishedAt', 'DESC');
	}


	/**
	 * @param string $phrase
	 * @return QueryBuilder
	 */
	public function getQuerySearchPosts($phrase)
	{
		$query = $this->getQueryPublishedPosts();
		return $query->andWhere(
						$query->expr()->orX(
								$query->expr()->like('p.title', $query->expr()->literal('%'.$phrase.'%')),
								$query->expr()->like('p.summary', $query->expr()->literal('%'.$phrase.'%')),
								$query->expr()->like('p.text', $query->expr()->literal('%'.$phrase.'%'))
						)
		);
	}


	/**
	 * @param string $slug
	 * @return QueryBuilder
	 */
	public function getQueryCategoryPosts($slug)
	{
		$query = $this->getQueryPublishedPosts();
		return $query
				->leftJoin('p.categories', 'c')
				->andWhere('c.slug = :slug')
				->setParameter('slug', $slug);
	}


	/**
	 * @param string $slug
	 * @return QueryBuilder
	 */
	public function getQueryTagPosts($slug)
	{
		$query = $this->getQueryPublishedPosts();
		return $query
				->leftJoin('p.tags', 't')
				->andWhere('t.slug = :slug')
				->setParameter('slug', $slug);
	}


	/**
	 * @return mixed[]
	 */
	public function findTagsCount()
	{
		return $this->createQueryBuilder('p')
						->select('COUNT(p) as tagCount')
						->addSelect('t.name')
						->addSelect('t.slug')
						->leftJoin('p.tags', 't')
						->groupBy('t.id')
						->where('p.active = :true')
						->andWhere('p.deleted >= :false')
						->andWhere('p.publishedAt <= :now')
						->andWhere('t.slug IS NOT NULL')
						->setParameter('true', true)
						->setParameter('false', false)
						->setParameter('now', new \DateTime)
						->orderBy('t.name', 'ASC')
						->getQuery()
						->getResult();
	}


	public function findWithGallery($id)
    {

    }

}
