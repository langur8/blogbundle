<?php

/**
 * Predek entit
 *
 * @author Vaclav Dlouhy <vdlouhy@atlas.cz>
 */

namespace dlouhy\BlogBundle\Entity;

abstract class EntityAbstract
{
	
	public function __toString()
	{
		if(isset($this->name)) {			
			return $this->name ? $this->name : '-';
		}
		if(isset($this->title)) {			
			return $this->title ? $this->title : '-';
		}	
		
		return '';
	}

}