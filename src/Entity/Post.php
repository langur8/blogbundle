<?php

namespace dlouhy\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="dlouhy\BlogBundle\Entity\Repository\PostRepository")
 * @ORM\Table(name="blog_posts", indexes={
 * 		@ORM\Index(name="idx_show", columns={"active", "deleted", "publishedAt"})
 * 		})
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("slug")
 *
 */
class Post extends EntityAbstract
{

	/**
	 * @var int
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $modified;

	/**
	 * @var bool
	 * @ORM\Column(type="boolean")
	 */
	protected $active = true;

	/**
	 * @var bool
	 * @ORM\Column(type="boolean")
	 */
	protected $deleted = false;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 * @Assert\NotBlank
	 */
	protected $title;

	/**
	 * @var string
	 * @ORM\Column(type="string", unique=true)
	 * @Assert\NotBlank
	 */
	protected $slug;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $metaKeywords;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $metaDescription;

	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $summary;

	/**
	 * @var string
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $text;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 * @Assert\DateTime
	 */
	protected $publishedAt;

	/**
	 * @var User
	 * @ORM\ManyToOne(targetEntity="dlouhy\UserBundle\Entity\User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
	 * */
	protected $author;
	
	/**
	 * @var Category[]
	 * @ORM\ManyToMany(targetEntity="Category", cascade={"persist"})
	 * @ORM\JoinTable(name="blog_post_categories")
	 * @ORM\OrderBy({"name": "ASC"})
	 */
	protected $categories;

	/**
	 * @var Tag[]|ArrayCollection
	 * @ORM\ManyToMany(targetEntity="Tag", cascade={"persist"})
	 * @ORM\JoinTable(name="blog_post_tag")
	 * @ORM\OrderBy({"name": "ASC"})
	 * @Assert\Count(max="4", maxMessage="post.too_many_tags")
	 */
	protected $tags;

	/**
	 * @var ImageGallery
	 * @ORM\OneToOne(targetEntity="dlouhy\ImageBundle\Entity\ImageGallery", cascade={"persist"}, fetch="EAGER")
	 * @ORM\JoinColumn(name="image_gallery_id", referencedColumnName="id")
	 */
	protected $imageGallery;


	/**
	 * @ORM\PreUpdate
	 */
	public function preUpdate()
	{
		$this->modified = new \DateTime;
	}


	/**
	 * @ORM\PrePersist
	 */
	public function prePersist()
	{
		$this->created = new \DateTime;
	}

//GENERATED

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Post
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Post
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Post
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Post
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Post
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return Post
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return Post
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set summary
     *
     * @param string $summary
     * @return Post
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary
     *
     * @return string 
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Post
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set publishedAt
     *
     * @param \DateTime $publishedAt
     * @return Post
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime 
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set author
     *
     * @param \dlouhy\UserBundle\Entity\User $author
     * @return Post
     */
    public function setAuthor(\dlouhy\UserBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \dlouhy\UserBundle\Entity\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Add categories
     *
     * @param \dlouhy\BlogBundle\Entity\Category $categories
     * @return Post
     */
    public function addCategory(\dlouhy\BlogBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \dlouhy\BlogBundle\Entity\Category $categories
     */
    public function removeCategory(\dlouhy\BlogBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add tags
     *
     * @param \dlouhy\BlogBundle\Entity\Tag $tags
     * @return Post
     */
    public function addTag(\dlouhy\BlogBundle\Entity\Tag $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \dlouhy\BlogBundle\Entity\Tag $tags
     */
    public function removeTag(\dlouhy\BlogBundle\Entity\Tag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set imageGallery
     *
     * @param \dlouhy\ImageBundle\Entity\ImageGallery $imageGallery
     * @return Post
     */
    public function setImageGallery(\dlouhy\ImageBundle\Entity\ImageGallery $imageGallery = null)
    {
        $this->imageGallery = $imageGallery;

        return $this;
    }

    /**
     * Get imageGallery
     *
     * @return \dlouhy\ImageBundle\Entity\ImageGallery 
     */
    public function getImageGallery()
    {
        return $this->imageGallery;
    }
}
