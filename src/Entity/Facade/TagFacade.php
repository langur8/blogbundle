<?php

namespace dlouhy\BlogBundle\Entity\Facade;

use Doctrine\Bundle\DoctrineBundle\Registry;
use dlouhy\BlogBundle\Entity\Tag;
use dlouhy\BlogBundle\Entity\Post;

class TagFacade extends FacadeAbstract
{

	protected $entityName = Tag::class;


	/**
	 * @param Registry $doctrine
	 */
	public function __construct(Registry $doctrine)
	{
		$this->doctrine = $doctrine;
	}


	/**
	 * @return Tag[]
	 */
	public function findOrderedList()
	{
		return $this->getRepository()->findBy([], ['name' => 'ASC']);
	}


	/**
	 * @return mixed[]
	 */
	public function findPublishedCountList()
	{
		return $this->doctrine->getRepository(Post::class)->findTagsCount();
	}

}
