<?php

namespace dlouhy\BlogBundle\Entity\Facade;

use Symfony\Bridge\Doctrine\RegistryInterface;
use dlouhy\BlogBundle\Entity\Post;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;

class PostFacade extends FacadeAbstract
{

	/**
	 * @var string
	 */
	protected $entityName = Post::class;

	/**
	 * @var PaginatorInterface $paginator
	 */
	private $paginator;


	/**
	 * @param RegistryInterface $doctrine
	 */
	public function __construct(RegistryInterface $doctrine, PaginatorInterface $paginator)
	{
		$this->doctrine = $doctrine;
		$this->paginator = $paginator;
	}


	/**
	 * @return Post[]
	 */
	public function findAll()
	{
		return $this->getRepository()->findAll();
	}


	/**
	 * @return Post
	 */
	public function find($id)
	{
		return $this->getRepository()->find($id);
	}


	/**
	 * @return Post
	 */
	public function getPost($slug)
	{
		return $this->getRepository()->findOneBy(['slug' => $slug, 'deleted' => false]);
	}


	/**
	 * @return SlidingPagination
	 */
	public function getPublishedPostsPagination($page, $postsPerPage)
	{
		$query = $this->getRepository()->getQueryPublishedPosts();
		return $this->paginator->paginate($query, $page, $postsPerPage);
	}


	/**
	 * @return SlidingPagination
	 */
	public function getSearchPostsPagination($phrase, $page, $postsPerPage)
	{
		$query = $this->getRepository()->getQuerySearchPosts($phrase);
		return $this->paginator->paginate($query, $page, $postsPerPage);
	}


	/**
	 * @return SlidingPagination
	 */
	public function getCategoryPostsPagination($slug, $page, $postsPerPage)
	{
		$query = $this->getRepository()->getQueryCategoryPosts($slug);
		return $this->paginator->paginate($query, $page, $postsPerPage);
	}


	/**
	 * @return SlidingPagination
	 */
	public function getTagPostsPagination($slug, $page, $postsPerPage)
	{
		$query = $this->getRepository()->getQueryTagPosts($slug);
		return $this->paginator->paginate($query, $page, $postsPerPage);
	}


	/**
	 * @return Post[]
	 */
	public function findSitemapPosts()
	{
		$query = $this->getRepository()->getQueryPublishedPosts();
		return $query->getQuery()->getResult();
	}


	/**
	 * @return Post[]
	 */
	public function findRecentPosts($limit = 10)
	{
		$query = $this->getRepository()->getQueryPublishedPosts();
		return $query->setMaxResults($limit)->getQuery()->getResult();
	}


	/**
	 * @return Post[]
	 */
	public function save(Post $post)
	{
		$em = $this->doctrine->getManager();
		$em->persist($post);
		$em->flush();
	}

}
