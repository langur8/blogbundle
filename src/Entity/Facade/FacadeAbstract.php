<?php

namespace dlouhy\BlogBundle\Entity\Facade;

use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\EntityRepository;

class FacadeAbstract
{

	/**
	 * @var RegistryInterface
	 */
	protected $doctrine;

	/**
	 * @var string
	 */
	protected $entityName;


	/**
	 * @return string
	 */
	public function getEntityName()
	{
		return $this->entityName;
	}


	/**
	 * @return EntityRepository
	 */
	protected function getRepository()
	{
		return $this->doctrine->getRepository($this->getEntityName());
	}

}
