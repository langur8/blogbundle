<?php

namespace dlouhy\BlogBundle\Entity\Facade;

use Symfony\Bridge\Doctrine\RegistryInterface;
use dlouhy\BlogBundle\Entity\Category;

class CategoryFacade extends FacadeAbstract
{

	protected $entityName = Category::class;


	/**
	 * @param RegistryInterface $doctrine
	 */
	public function __construct(RegistryInterface $doctrine)
	{
		$this->doctrine = $doctrine;
	}


	/**
	 * @return Category[]
	 */
	public function findOrderedList()
	{
		return $this->getRepository()->findBy([], ['name' => 'ASC']);
	}

}
