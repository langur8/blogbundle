<?php

namespace dlouhy\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use dlouhy\UtilsBundle\Utils\Strings;

/**
 * @ORM\Entity(repositoryClass="dlouhy\BlogBundle\Entity\Repository\TagRepository")
 * @ORM\Table(name="blog_tags")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("slug")
 */
class Tag extends EntityAbstract implements \JsonSerializable
{
    /**
     * @var int
	 *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
	 * @Assert\NotBlank
     * @ORM\Column(type="string", unique=true)
     */
    protected $name;

	/**
	 * @var string
	 * @ORM\Column(type="string", unique=true)
	 * @Assert\NotBlank
	 */
	protected $slug;

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        // This entity implements JsonSerializable (http://php.net/manual/en/class.jsonserializable.php)
        // so this method is used to customize its JSON representation when json_encode()
        // is called, for example in tags|json_encode (app/Resources/views/form/fields.html.twig)

        return $this->name;
    }

	/**
	 * @ORM\PreFlush
	 */
	public function preFlush()
	{
		$this->slug = Strings::Slug($this->name);
	}


	//GENERATED

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Tag
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
