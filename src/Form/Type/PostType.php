<?php

namespace dlouhy\BlogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use dlouhy\BlogBundle\Entity\Post;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use dlouhy\BlogBundle\Form\Type\TagsInputType;
use Doctrine\ORM\EntityRepository;

class PostType extends AbstractType
{


	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{

		$builder
				->add('title', TextType::class)
				->add('slug', TextType::class)
				->add('metaKeywords', TextType::class, [
					'required' => false
					])
				->add('metaDescription', TextType::class, [
					'required' => false
					])
				->add('publishedAt', DateTimeType::class)
				->add('summary', TextareaType::class, [
					'required' => false
					])
				->add('text', TextareaType::class, [
					'attr' => [
						'class' => 'tinymce',
						'rows' => 30
						]
					])
				->add('active', CheckboxType::class, [
					'required' => false
					])
				->add('tags', TagsInputType::class, [
					'required' => false
					])
				->add('categories', EntityType::class, [
					'class' => 'dlouhyBlogBundle:Category',
					'choice_label' => 'name',
					'multiple' => true,
					'expanded' => true,
					'required' => false
				])
				->add('author', EntityType::class, [
					'class' => 'dlouhyUserBundle:User',
					'query_builder' => function (EntityRepository $er) {
						return $er->createQueryBuilder('u')->orderBy('u.name', 'ASC');
					},
					'choice_label' => 'name',
				])
				->add('save', SubmitType::class);
	}


	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => Post::class,
		]);
	}

}
