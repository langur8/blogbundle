<?php
namespace dlouhy\BlogBundle\ControllerFront;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use dlouhy\BlogBundle\Entity\Facade\PostFacade;
use dlouhy\BlogBundle\Entity\Facade\TagFacade;
use dlouhy\BlogBundle\Entity\Facade\CategoryFacade;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use dlouhy\BlogBundle\Entity\Post;



class BlogController extends Controller
{

	/**
	 * @var int
	 */
	private $postsPerPage = 4;

	/**
	 * @var int
	 */
	private $page;

	/**
	 * @Route("/blog", name="blog_index")
	 */
	public function indexAction(Request $request)
	{
		$this->page = $request->get('page', 1);
		return $this->renderIndex($this->getPostFacade()->getPublishedPostsPagination($this->page, $this->postsPerPage));
	}


	/**
	 * @Route("/blog/tag/{slug}", name="blog_tag", requirements={"slug": "[a-zA-Z0-9\-_\/]+"})
	 */
	public function tagAction(Request $request, $slug)
	{
		$this->page = $request->get('page', 1);
		return $this->renderIndex($this->getPostFacade()->getTagPostsPagination($slug, $this->page, $this->postsPerPage));
	}


	/**
	 * @Route("/blog/category/{slug}", name="blog_category", requirements={"slug": "[a-zA-Z0-9\-_\/]+"})
	 */
	public function categoryAction(Request $request, $slug)
	{
		$this->page = $request->get('page', 1);
		return $this->renderIndex($this->getPostFacade()->getCategoryPostsPagination($slug, $this->page, $this->postsPerPage));
	}


	/**
	 * @Route("/blog/search", name="blog_search")
	 */
	public function searchAction(Request $request)
	{
		$phrase = trim($request->get('s', ''));
		$this->page = $request->get('page', 1);
		return $this->renderIndex($this->getPostFacade()->getSearchPostsPagination($phrase, $this->page, $this->postsPerPage));
	}


	/**
	 *
	 * @param SlidingPagination $pagination
	 * @return Resp
	 */
	private function renderIndex(SlidingPagination $pagination)
	{
		$noResults = false;
		if($pagination->count() === 0) {
			$pagination = $this->getPostFacade()->getPublishedPostsPagination($this->page, $this->postsPerPage);
			$noResults = true;
		}
		return $this->render('dlouhyBlogBundle:Blog:index.html.twig', [
			'pagination' => $pagination,
			'categories' =>	$this->getCategoryFacade()->findOrderedList(),
			'tags' => $this->getTagFacade()->findPublishedCountList(),
			'recentPosts' => $this->getPostFacade()->findRecentPosts(),
			'noResults' => $noResults
		]);
	}


	/**
	 * @Route("/blog/{slug}", name="blog_view", requirements={"slug": "[a-zA-Z0-9\-_\/]+"})
	 */
	public function viewAction($slug)
	{
		$post = $this->getPostFacade()->getPost($slug);
		if(!$post instanceof Post) {
			throw $this->createNotFoundException();
		}
		return $this->render('dlouhyBlogBundle:Blog:view.html.twig', [
			'post' => $post,
			'categories' =>	$this->getCategoryFacade()->findOrderedList(),
			'tags' => $this->getTagFacade()->findPublishedCountList(),
			'recentPosts' => $this->getPostFacade()->findRecentPosts()
		]);
	}


	/**
	 * @return PostFacade
	 */
	private function getPostFacade()
	{
		return $this->get('dlouhy_blog.facade.post');
	}


	/**
	 * @return TagFacade
	 */
	private function getTagFacade()
	{
		return $this->get('dlouhy_blog.facade.tag');
	}


	/**
	 * @return CategoryFacade
	 */
	private function getCategoryFacade()
	{
		return $this->get('dlouhy_blog.facade.category');
	}

}