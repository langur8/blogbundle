<?php

namespace dlouhy\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use dlouhy\BlogBundle\Entity\Post;
use dlouhy\BlogBundle\Form\Type\PostType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use dlouhy\BlogBundle\Entity\Facade\TagFacade;
use dlouhy\BlogBundle\Entity\Facade\PostFacade;

class PostController extends Controller
{

	use \dlouhy\ImageBundle\Controller\BaseGalleryControllerTrait;

	/**
	 * @Route("/posts", name="admin_post_list")
	 */
	public function listAction()
	{
		return $this->render('dlouhyBlogBundle:Post:list.html.twig', ['records' => $this->getPostFacade()->findAll()]);
	}


	/**
	 * @Route("/post/{id}", name="admin_post_view", requirements={"id": "\d+"})
	 */
	public function viewAction($id)
	{
		$post = $this->getPostFacade()->find($id);

		if (!$post instanceof Post) {
			throw $this->createNotFoundException('Not found');
		}
		return $this->render('dlouhyBlogBundle:Post:view.html.twig', ['post' => $post]);
	}


	/**
	 * @Route("/post/add", name="admin_post_add")
	 */
	public function addAction()
	{
		$post = new Post;
		$post->setAuthor($this->get('security.context')->getToken()->getUser());
		$post->setPublishedAt(new \DateTime);
		$form = $this->createForm(PostType::class, $post, ['action' => $this->generateUrl('admin_post_save')]);
		return $this->render('dlouhyBlogBundle:Post:add.html.twig', ['form' => $form->createView(), 'tags' => $this->getTagFacade()->findOrderedList()]);
	}


	/**
	 * @Route("/post/edit/{id}", name="admin_post_edit")
	 */
	public function editAction($id)
	{
		$post = $this->getPostFacade()->find($id);

		if (!$post instanceof Post) {
			throw $this->createNotFoundException('Not found');
		}
		$form = $this->createForm(PostType::class, $post, ['action' => $this->generateUrl('admin_post_save', ['id' => $id])]);
		return $this->render('dlouhyBlogBundle:Post:add.html.twig', ['form' => $form->createView(), 'tags' => $this->getTagFacade()->findOrderedList()]);
	}


	/**
	 * @Route("/post/save/{id}", name="admin_post_save", defaults={"id" = null})
	 */
	public function saveAction(Request $request, $id)
	{
		if (!$request->isXmlHttpRequest()) {
			return new JsonResponse(['message' => 'You can access this only using Ajax!'], 400);
		}

		if ($id > 0) {
			$repo = $this->getDoctrine()->getRepository('dlouhy\BlogBundle\Entity\Post');
			$post = $repo->find($id);
			if (!$post instanceof Post) {
				return $this->createNotFoundException('Not found');
			}
		} else {
			$post = new Post;
		}

		$form = $this->createForm(PostType::class, $post, ['action' => $this->generateUrl('admin_post_save', ['id' => $id])]);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$this->getPostFacade()->save($post);
			$returnCode = 200;
			$returnMessage = '';
			$redirect = $this->generateUrl('admin_post_image_gallery', ['id' => $post->getId()]);
		} else {
			$returnCode = 400;
			$returnMessage = 'Error';
			$redirect = false;
		}
		return new JsonResponse([
			'redirect' => $redirect,
			'html' => $this->renderView('dlouhyBlogBundle:Post:form_content.html.twig', [
				'tags' => $this->getTagFacade()->findOrderedList(),
				'form' => $form->createView()
			])
				], $returnCode);
	}


	/**
	 * @Route("/post/delete/{id}", name="admin_post_delete", requirements={"id": "\d+"})
	 */
	public function deleteAction(Request $request, $id)
	{		
		$post = $this->getPostFacade()->find($id);
		$em = $this->getDoctrine()->getManager();
		$em->remove($post);
		$em->flush();

		if ($request->isXmlHttpRequest()) {
			return new JsonResponse(array('id' => $id), 200);
		} else {
			return $this->redirect($this->generateUrl('admin_post_list'));
		}
	}


	/**
	 * @Route("/post/switch_active/{id}", name="admin_post_switch_active", requirements={"id": "\d+"})
	 */
	public function switchActiveAction(Request $request, $id)
	{		
		$post = $this->getPostFacade()->find($id);
		if (!$post) {
			throw $this->createNotFoundException('Prispevek neexistuje: ' . $id);
		}
		$post->setActive(!$post->getActive());

		$em = $this->getDoctrine()->getManager();
		$em->persist($post);
		$em->flush();

		if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'elementId' => 'data-table-content',
                'html' => $this->renderView('dlouhyBlogBundle:Post:table.html.twig', [
                    'records' =>  $this->getPostFacade()->findAll(),
                    'includeJs' => true
                ])], 200);
		} else {
			return $this->redirect($this->generateUrl('admin_post_list'));
		}
	}


	/**
	 * @return PostFacade
	 */
	private function getPostFacade()
	{
		return $this->get('dlouhy_blog.facade.post');
	}


	/**
	 * @return TagFacade
	 */
	private function getTagFacade()
	{
		return $this->get('dlouhy_blog.facade.tag');
	}


	/**
	 * @Route("/post/image-gallery/images/{id}", name="admin_post_image_gallery")
	 */
	public function galleryAction(Request $request, $id)
	{
		return $this->baseGalleryAction($request, $id);
	}


	/**
	 * @Route("/post/image-gallery/images/save/{id}", name="admin_post_image_gallery_save")
	 */
	public function saveGalleryAction(Request $request, $id)
	{
		$this->init($request, $id);
		$this->redirect = $this->generateUrl('admin_post_list');
		return $this->baseSaveGalleryAction($request, $id);
	}


	/**
	 * @Route("/post/image-gallery/upload-images/{id}", name="admin_post_image_gallery_upload", requirements={"id": "\d+"})
	 */
	public function uploadImagesAction(Request $request, $id)
	{
		return $this->baseUploadImages($request, $id);
	}


	/**
	 * @Route("/post/image-gallery/delete-image/{id}/{imageId}", name="admin_post_image_gallery_delete", requirements={"id": "\d+", "imageId": "\d+"})
	 */
	public function deleteImageAction(Request $request, $id, $imageId)
	{
		return $this->baseDeleteImage($request, $id, $imageId);
	}

}
