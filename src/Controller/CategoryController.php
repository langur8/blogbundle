<?php

namespace dlouhy\BlogBundle\Controller;

use dlouhy\SimpleCRUDBundle\Controller\BaseCRUDController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use dlouhy\BlogBundle\Entity\Category;
use dlouhy\BlogBundle\Form\CategoryType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CategoryController extends BaseCRUDController
{

    /**
     * @var string
     */
    protected $sEntity = 'dlouhy\BlogBundle\Entity\Category';

	/**
	 * @Route("/post-categories", name="admin_post_category_list")
	 */
	public function listAction(Request $request)
	{
        return parent::listAction($request);
	}


	/**
	 * @Route("/post-category/add", name="admin_post_category_add")
	 */
	public function addAction(Request $request)
	{
        return parent::addAction($request);
	}


	/**
	 * @Route("/post-category/edit/{id}", name="admin_post_category_edit")
	 */
    public function editAction(Request $request, $id)
    {
        return parent::editAction($request, $id);
    }


	/**
	 * @Route("/post-category/save/{id}", name="admin_post_category_save", defaults={"id" = null})
	 */
	public function saveAction(Request $request, $id)
    {
        return parent::saveAction($request, $id);
    }


	/**
	 * @Route("/post-category/delete/{id}", name="admin_post_category_delete")
	 */
	public function deleteAction(Request $request, $id)
	{
        return parent::deleteAction($request, $id);
	}


	/**
	 * @Route("/post-category/switch-sactive/{id}", name="admin_post_category_switch_active")
	 */
	public function switchActiveAction(Request $request, $id)
	{
        return parent::switchBoolAction($request, $id, 'active');
	}

}
