<?php

namespace dlouhy\BlogBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Knp\Menu\MenuItem;

class BreadcrumbMenu extends ContainerAware
{

    public function create(FactoryInterface $factory, array $options)
    {

        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'breadcrumb');
        $menu->setChildrenAttribute('xmlns:v', 'http://rdf.data-vocabulary.org/#');

        $nodes = [
            $this->container->getParameter('project')['name'] => ['uri' => $this->container->get('request')->getBaseUrl()],
            'Blog' => ['route' => 'blog_index']
        ];

        $post = $options['entity'];
        if(!empty($post)) {
            $nodes[$post->getTitle()] = ['route' => 'blog_view', 'routeParameters' => ['slug' => $post->getSlug()]];
        }

        $node = $menu;
        $n = 0;
        foreach ($nodes as $name => $options) {
            $node = $node->addChild($name, $options);
            $node->setAttribute('rel', 'v:child');
            $node->setAttribute('typeof', 'v:Breadcrumb');
            $node->setLinkAttribute('rel', 'v:url');
            $node->setLinkAttribute('property', 'v:title');
            if ($n++) {
                $node->setAttribute('class', 'separator');
            }
        }

        return $menu;
    }


}
